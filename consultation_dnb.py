import explore_dnb as dnb

# Ici vos fonctions dédiées aux interactions
def afficher_menu(titre, liste_options):
    """Affiche un menu avec un titre au milieu et une liste d'options

    Args:
        titre (str): titre du menu
        liste_options (list): liste d'options
    """    
    cpt=0
    print('+'+'-'*(len(titre)+2)+'+')
    print('| '+titre+' |')
    print('+'+'-'*(len(titre)+2)+'+')
    for i in range(len(liste_options)):
        cpt+=1
        print(str(cpt)+' -> '+liste_options[i])


def demander_nombre(message, borne_max):
    """Demande grace a un message un nombre compris entre 1 et la borne max a l'utilisateur

    Args:
        message (str): message dit a l'utilisateur
        borne_max (int): nombre maximum a entrez

    Returns:
        int: Renvoie le nombre choisis par l'utilisateur, si le nombre n'est pas compris entre 1 et la borne maximale, la fonction renvoie None
    """    
    nb=input(message)
    if nb.isdecimal():
        nb=int(nb)
        if 1<=nb<=borne_max:
            return nb
    return None


def menu(titre, liste_options):
    """Affiche un menu avec un titre et une liste d'options

    Args:
        titre (str): Titre du menu
        liste_options (list): liste d'options

    Returns:
        int: Renvoie le nombres que l'utilisateur a choisis comme option
    """    
    afficher_menu(titre,liste_options)
    return demander_nombre('Quel option choissisez-vous ?\n',len(liste_options))



# ici votre programme principal
def programme_principal():
    """Permet le dialogue entre la machine et l'utilisateur
    """ 
    #Initialisation de l'état des menus et de la liste des fichiers chargé   
    fin=False
    fin_fichier=False
    fin_principal=False
    fin_filtre=False
    fin_taux=False
    fin_autre=False
    list_fichier=[]
    while not fin: #Boucle principale pour afficher touts les menus

        #Menu chargement des fichiers
        liste_options= ["Charger un fichier seul",
                        "Fusionner un fichier avec ceux déjà chargé",
                        "Verifier que le fichier est bien triée",
                        "Voir les fichiers déjà chargé",
                        "Choix options",
                        "Quitter"]
        while not fin_fichier:
            rep = menu("Choix fichiers", liste_options)
            if rep is None:
                print(" \033[91m Cette option n'existe pas \033[0m")
            elif rep == 1: #Charge un fichier seulement et supprime tout les anciens
                try:
                    print("Vous avez choisi", liste_options[rep - 1])
                    nom_fic=input("Quel fichier faut-'il charger ?\n")
                    if dnb.est_bien_triee(dnb.charger_resultats(nom_fic)):
                        fic=dnb.charger_resultats(nom_fic)
                        list_fichier=[nom_fic]
                    else:
                        print("\033[91mLe fichier n'est pas triée\033[0m")
                except:
                    print("\033[91mLe fichier n'existe pas\033[0m")
            elif rep == 2: #Charge un nouveau fichiers et le fusionne avec ceux déjà chargé
                try:
                    
                    print("Vous avez choisi", liste_options[rep - 1])
                    nom_fic=input("Quel fichier faut-'il charger ?\n")
                    if dnb.est_bien_triee(dnb.charger_resultats(nom_fic)):
                        if nom_fic not in list_fichier:
                            fic=dnb.fusionner_resultats(fic,dnb.charger_resultats(nom_fic))
                            list_fichier.append(nom_fic)
                        else: 
                            print("\033[91mLe fichier est déjà charger\033[0m")
                    else:
                        print("\033[91mLe fichier n'est pas triée\033[0m")
                except:
                    if len(list_fichier)==0:
                        print("\033[91mIl faut avoir déjà charger un fichier\033[0m")
                    else:
                            print("\033[91mLe fichier n'existe pas\033[0m")
            elif rep == 3: #Verifie si un fichier est bien triée
                try:
                    print("Vous avez choisi", liste_options[rep - 1])
                    nom_fic_trier=input("Quel fichier faut-'il vérifier ?\n")
                    fic_trier=dnb.charger_resultats(nom_fic_trier)
                    if dnb.est_bien_triee(fic_trier):
                        print("Ce fichier est bien triée")
                    else: print("Ce fichier n'est pas triée")
                except:
                    print("\033[91mLe fichier n'existe pas\033[0m")
            elif rep == 4: #Affiche les différents fichiers chargé et fusionné
                print(list_fichier)
            elif rep == 5: #Dirige vers le menu du choix des options
                if len(list_fichier)>0:
                    fin_fichier=True
                    fin_principal=False
                    fin_filtre=True
                    fin_taux=True
                    fin_autre=True
                    break
                else:
                    print("\033[91mVeuillez charger un fichier\033[0m")
            else: #Quitte l'IHM
                fin=True
                fin_fichier=True
                fin_principal=True
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            input("Appuyer sur Entrée pour continuer")



        #Menu Choix des options
        liste_options = ["Choix fichiers",
                        "Filtres",
                        "Taux",
                        "Autres",
                        "Quitter"]
        while not fin_principal:
            rep = menu("Choix options", liste_options)
            if rep is None:
                print("\033[91mCette option n'existe pas\033[0m")
            elif rep == 1: #Dirige vers le menu du choix des fichiers
                fin_fichier=False
                fin_principal=True
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            elif rep == 2: #Dirige vers le menu des options de filtrages
                fin_fichier=True
                fin_principal=True
                fin_filtre=False
                fin_taux=True
                fin_autre=True
                break
            elif rep == 3:  #Dirige vers le menu des options relativs aux taux
                fin_fichier=True
                fin_principal=True
                fin_filtre=True
                fin_taux=False
                fin_autre=True
                break
            elif rep == 4: #Dirige vers le menu des autres options
                fin_fichier=True
                fin_principal=True
                fin_filtre=True
                fin_taux=True
                fin_autre=False
                break
            else: #Quitte l'IHM
                fin=True
                fin_fichier=True
                fin_principal=True
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            input("Appuyer sur Entrée pour continuer")



        #Menu choix des filtres
        liste_options = ["Précédent",
                        "Filtre les résultats avec la session demandée",
                        "Filtre les résultats avec le département demandée",
                        "Filtre les résultats avec le/les college(s) demandée",
                        "Quitter"]
        while not fin_filtre:
            rep = menu("Filtres", liste_options)
            if rep is None:
                print("\033[91mCette option n'existe pas\033[0m")
            elif rep == 1: #Dirige vers le menu du choix des options
                fin_fichier=True
                fin_principal=False
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            elif rep == 2: #Affiche le(s) fichier(s) chargée en appliquant un filtre de session
                print("Vous avez choisi", liste_options[rep - 1])
                session=input("De quel session voulez-vous voir les résultats ?\n")
                print(dnb.filtre_session(fic,session))
            elif rep == 3: #Affiche le(s) fichier(s) chargée en appliquant un filtre de département
                print("Vous avez choisi", liste_options[rep - 1])
                departement=input("De quel département voulez-vous voir les résultats ?\n")
                print(dnb.filtre_departement(fic,departement))
            elif rep == 4: #Affiche le(s) fichier(s) chargée en appliquant un filtre de nom et de département
                print("Vous avez choisi", liste_options[rep - 1])
                nom=input("De quel college voulez-vous voir les résultats ?\n")
                departement=input("Situé dans quel département ?\n")
                print(dnb.filtre_college(fic,nom,departement))
            else: #Quitte l'IHM
                fin=True
                fin_fichier=True
                fin_principal=True
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            input("Appuyer sur Entrée pour continuer")


        #Menu choix des options relatifs aux taux
        liste_options = ["Précédent",
                        "Rechercher le meilleur taux de réussite",
                        "Rechercher le pire taux de réussite",
                        "Affiche le taux de réussie globale d'une session",
                        "Affiche le meilleur college d'une session",
                        "Affiche la moyenne du taux de réussite d'un college",
                        "Affiche la plus longue periode d'amélioration du taux de réussite global",
                        "Quitter"]
        while not fin_taux:
            rep = menu("Taux", liste_options)
            if rep is None:
                print("\033[91mCette option n'existe pas\033[0m")
            elif rep == 1: #Dirige vers le menu du choix des options
                fin_fichier=True
                fin_principal=False
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            elif rep == 2: #Affiche le meilleur taux de réussite du/des fichier(s) chargé(s)
                print("Vous avez choisi", liste_options[rep - 1])
                print( f'Le Meilleurs taux de réussite est : {"%.2f" % dnb.meilleur_taux_reussite(fic)} %')
            elif rep == 3: #Affiche le pire taux de réussite du/des fichier(s) chargé(s)
                print("Vous avez choisi", liste_options[rep - 1])
                print( f'Le pire taux de réussite est : {"%.2f" % dnb.pire_taux_reussite(fic)} %')    
            elif rep == 4: #Affiche le taux de réussite global d'une session seulement
                print("Vous avez choisi", liste_options[rep - 1])
                session=input("De quel session voulez-vous voir les résultats ?\n")
                if dnb.taux_reussite_global(fic,session) == None:
                    print("\033[91mLa session saisie est invalide\033[0m")
                else:
                    print( f'Le taux de réussite globale de la session {session} est : {"%.2f" % dnb.taux_reussite_global(fic,session)} %')
            elif rep == 5: #Affiche le meilleur collège de la session
                print("Vous avez choisi", liste_options[rep - 1])
                session=input("De quel session voulez-vous voir les résultats ?\n")
                if dnb.meilleur_college(fic,session) == None:
                    print("\033[91mLa session saisie est invalide\033[0m")
                else:
                    print(f'Le meilleur college dans la session {session} est : {dnb.meilleur_college(fic,session)}')
            elif rep == 6: #Affiche la moyenne du taux de réussite d'un collège où l'on donne son nom et son département
                print("Vous avez choisi", liste_options[rep - 1])
                nom=input("De quel college voulez-vous voir les résultats ?\n")
                departement=input("Situé dans quel département ?\n")
                if dnb.moyenne_taux_reussite_college(fic,nom,departement) == None:
                    print("\033[91mLe nom ou le département saisie est invalide\033[0m")
                else:
                    print(f'La moyenne du taux de réussite du/des college(s) {nom} dans le departement {departement} est : {"%.2f" % dnb.moyenne_taux_reussite_college(fic,nom,departement)} %')
            elif rep == 7: #Affiche la date de début et la date de fin de la plus longue période d'amélioration du taux de réussite global
                print("Vous avez choisi", liste_options[rep - 1])
                print(f'La plus longue période d\'amélioraration est : {dnb.plus_longe_periode_amelioration(fic)}')        
            else: #Quitte l'IHM
                fin=True
                fin_fichier=True
                fin_principal=True
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            input("Appuyer sur Entrée pour continuer")



        #Menu choix "autre", ceux qui n'avait pas leur place dans les autres catégories
        liste_options = ["Précédent",
                        "Calculer le nombre total de candidats admis et de candidats présents aux épreuves",                        
                        "Affiche la liste des sessions qui contiennent au moins un résultat",
                        "Quitter"]
        while not fin_autre:
            rep = menu("Autres", liste_options)
            if rep is None:
                print("\033[91mCette option n'existe pas\033[0m")
            elif rep == 1: #Dirige vers le menu du choix des options
                fin_fichier=True
                fin_principal=False
                fin_filtre=True
                fin_taux=True
                fin_autre=True
                break
            elif rep == 2: #Affiche le total d'admis et le total de présent du/des fichier(s) chargé(s)
                print("Vous avez choisi", liste_options[rep - 1])
                print(f'Le nombre total d\'admis et de présent sont : {dnb.total_admis_presents(fic)}')
            elif rep == 3: #Affiche la liste de toute les années où il y a eu au moins un résultat
                print("Vous avez choisi", liste_options[rep - 1])
                print(f'Les sessions avec au moins un résultat sont : {dnb.liste_sessions(fic)}')
            else: #Quitte l'IHM
                fin=True
                fin_fichier=True
                fin_principal=True
                fin_filtre=True
                fin_taux=True
                break
            input("Appuyer sur Entrée pour continuer")

            
    print("Merci au revoir!")


programme_principal()
